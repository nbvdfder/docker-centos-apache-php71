#!/usr/bin/env bash

# paths and files
configPrivateKey="/opt/mydocker/config/ssh/id_rsa"
configPublicKey="/opt/mydocker/config/ssh/id_rsa.pub"

containerPrivateKey="/root/.ssh/id_rsa"
containerPublicKey="/root/.ssh/id_rsa.pub"


userFile="/opt/pubkey/id_rsa.pub"

# function delete container ssh-keys
delete_container_ssh() {
    echo -e "\e[32mSearching for existing rsa files in /root/.ssh/ ...\e[0m"

    if [ -f $containerPrivateKey ]
    then
        echo -e "\e[31mPrivate Key found, try to delete ...\e[0m"
        rm -f $containerPrivateKey

        # check success
        if [ ! -f $containerPrivateKey ]
        then
            echo -e "\e[32mOld private-key deleted.\e[0m"
        else
            echo -e "\e[31mDelete of old private-key failed.\e[31m"
        fi
    else
        echo -e "\e[32mNo private-key found.\e[0m"
    fi

    if [ -f $containerPublicKey ]
    then
        echo -e "\e[31mPublic Key found, try to delete ...\e[0m"
        rm -f $containerPublicKey

        # check success
        if [ ! -f $containerPublicKey ]
        then
            echo -e "\e[32mOld public-key deleted.\e[0m"
        else
            echo -e "\e[31mDelete of old public-key failed.\e[31m"
        fi
    else
        echo -e "\e[32mNo public-key found.\e[0m"
    fi
}

# check if ssh-files exists in /root/.ssh/
check_container_ssh() {
    echo -e "\e[32mCheck existing rsa files ...\e[0m"

    if [ -f $containerPrivateKey ]
    then
        echo -e "\e[32mPrivate-key found.\e[0m"
    else
        echo -e "\e[31mNo private-key found.\e[0m"
    fi

    if [ -f $containerPublicKey ]
    then
        echo -e "\e[32mPublic-key found.\e[0m"
    else
        echo -e "\e[31mNo public-key found.\e[0m"
    fi
}

show_public_key() {
    echo -e "#####################################################################################\n\e[0m"
    echo -e "\e[31mThis is your public key, use it for authentication for example in git.\n"
    cat $containerPublicKey
    echo -e "\n\e[0m"
}

# make ssh-directory
mkdir /root/.ssh

# check if keys are available in config/ssh/
if [ -f $configPrivateKey ] && [ -f $configPublicKey ]
then
    # Copy available keys to container, clear all old files
    echo -e "\e[32mSSH-Keys found, try to copy to /root/.ssh/ ...\e[0m"

    # clean ssh-directory
    delete_container_ssh

    cp $configPrivateKey /root/.ssh/
    cp $configPublicKey /root/.ssh/

    # check if copy successful
    echo -e "\e[32mSearching for copied files ...\e[0m"
    check_container_ssh

    # change rights
    chmod 600 /root/.ssh/id_rsa
    chmod 600 /root/.ssh/id_rsa.pub
else
    echo -e "\e[31mNo SSH-Key-Pair found in config/ssh !\e[0m"

    # Deleting old ssh-files
    delete_container_ssh

    # Generating new ssh-keys
    echo -e "\e[32mTry to generate new key-pair ...\e[0m"
    ssh-keygen -t rsa -N "" -f /root/.ssh/id_rsa

    # check success
    echo -e "\e[32mChecking success ...\e[0m"
    check_container_ssh
fi

# show public key
show_public_key