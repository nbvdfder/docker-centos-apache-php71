FROM centos:latest

MAINTAINER Jacob Lohse <lohse.jacob@googlemail.com>

ENV container docker

# install epel and webtatic repos
RUN rpm -Uvh https://dl.fedoraproject.org/pub/epel/epel-release-latest-7.noarch.rpm \
&& rpm -Uvh https://mirror.webtatic.com/yum/el7/webtatic-release.rpm

RUN yum -y update

# install php and apache
RUN yum -y install mod_php71w php71w-opcache php71w-cli php71w-common php71w-gd php71w-intl php71w-mbstring php71w-mcrypt php71w-mysql php71w-mssql php71w-pdo php71w-pear php71w-soap php71w-xml php71w-xmlrpc httpd

RUN rm -rf /etc/localtime \
 && ln -s /usr/share/zoneinfo/Europe/Berlin /etc/localtime

# new directory for zend-installations (see v-host config)
RUN mkdir -p /var/www/page/public

# copy all configs and vendor to docker /tmp

RUN mkdir -p /opt/mydocker/config \
 && mkdir -p /opt/mydocker/vendor

COPY config /opt/mydocker/config/
COPY vendor /opt/mydocker/vendor/

# ssh-client and keys
RUN yum -y install openssh-clients

# generate keys
RUN bash /opt/mydocker/config/ssh/ssh-install.sh

# install composer (global)
RUN yum -y install wget
RUN bash /opt/mydocker/vendor/composer/autoinstall_composer.sh
RUN mv composer.phar /usr/local/bin/composer

# clean up
RUN yum clean all

# portforwarding
EXPOSE 80

VOLUME ["/var/log/httpd"]

# enable httpd
RUN systemctl enable httpd

CMD ["/usr/sbin/init"]
